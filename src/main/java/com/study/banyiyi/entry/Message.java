package com.study.banyiyi.entry;

import lombok.Builder;
import lombok.Data;

/**
 * @ClassName Message
 * @Description 创建消息实体
 * @Author yangwm
 * @Date 2021/7/26 10:46
 * @Version 1.0
 */
@Data
@Builder
public class Message {

    private String title;
    private String msg;
    private String code;
    private String sender;
    private String sentTime;
}
