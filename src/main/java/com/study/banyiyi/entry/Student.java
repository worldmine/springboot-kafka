package com.study.banyiyi.entry;

import lombok.Builder;
import lombok.Data;

/**
 * @ClassName Student
 * @Description TODO
 * @Author yangwm
 * @Date 2021/7/5 11:15
 * @Version 1.0
 */
@Data
public class Student {

    private String name;
    private String grand;
    private Integer age;
    private String studentNo;
    public String msg;

    public Student(){
        System.out.println("我是不带参数、public修饰构造器");
    }

    public Student(String name,Integer age){
        this.name = name;
        this.age = age;
        System.out.println("我是带参、public修饰构造器，参数分别是"+name+","+age);
    }

    private Student(String name){
        this.name = name;
        System.out.println("我是带参、private修饰构造器，参数是："+name);
    }

    protected Student(String name,Integer age,String grand){
        this.name = name;
        this.age = age;
        this.grand = grand;
        System.out.println("我是带参、protected修饰的构造器，参数是："+name + "," + age + "," +grand);
    }

    static {
        System.out.println("我是student实体，被执行了static初始化了！！");
    }

    public void score(){
        System.out.println("我是public修饰方法");
    }

    protected void failScore(){
        System.out.println("我是protected修饰方法");
    }

    private void successScore(){
        System.out.println("我是priviate修饰方法");
    }

    private void  outPutMessage(String message){
        System.out.println("我是outPutMessage方法，我的使命时将你输入内容原样输出，输入内容是："+message);
    }
}
