package com.study.banyiyi.config;

import com.google.gson.GsonBuilder;
import com.study.banyiyi.entry.Message;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;

/**
 * @ClassName KafkaProducer
 * @Description 生产者
 * @Author yangwm
 * @Date 2021/7/26 10:55
 * @Version 1.0
 */
@Slf4j
@Component
public class KafkaProducer {

    @Autowired
    private KafkaTemplate<String,String> kafkaTemplate;

    @Value("${kafka.topic.user}")
    private String topicName;

    public void sendMessage(Message message){
        GsonBuilder gson = new GsonBuilder();
        gson.setPrettyPrinting();
        gson.setDateFormat("yyyy-mm-dd HH:mm:ss");
        String toJson = gson.create().toJson(message);
        kafkaTemplate.send(topicName,toJson);
        log.info("生产者发送消息至kafka：" + message);
    }

}
