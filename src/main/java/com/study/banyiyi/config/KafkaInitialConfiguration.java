package com.study.banyiyi.config;

import org.apache.kafka.clients.admin.NewTopic;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @ClassName KafkaInitialConfiguration
 * @Description kafka初始化默认主题
 * @Author yangwm
 * @Date 2021/7/27 17:20
 * @Version 1.0
 */
@Configuration
public class KafkaInitialConfiguration {

    @Bean
    public NewTopic initTopic(){
        //创建一个主题名称为：topic1  分区数为10，分区副本为2
        return new NewTopic("topic1",10,(short)1);
    }
}
