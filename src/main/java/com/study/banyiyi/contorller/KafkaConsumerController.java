package com.study.banyiyi.contorller;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @ClassName KafkaConsumerController
 * @Description 消费者控制层
 * @Author yangwm
 * @Date 2021/7/27 18:19
 * @Version 1.0
 */
@RestController
@RequestMapping("/kafka/consumer")
public class KafkaConsumerController {

    @KafkaListener(topics = {"topic1"})
    public void receiveMessage(ConsumerRecord<String,Object> record){
        System.out.println("简单消费：" + record.topic() +":" + record.value());
    }

}
