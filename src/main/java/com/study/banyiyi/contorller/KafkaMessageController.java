package com.study.banyiyi.contorller;

import com.study.banyiyi.config.KafkaConsumer;
import com.study.banyiyi.config.KafkaProducer;
import com.study.banyiyi.entry.Message;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @ClassName KafkaMessageController
 * @Description 消息控制器
 * @Author yangwm
 * @Date 2021/7/26 11:06
 * @Version 1.0
 */
@RestController
@RequestMapping("/kafka")
public class KafkaMessageController {

    @Autowired
    private KafkaProducer kafkaProducer;

    @Autowired
    private KafkaConsumer kafkaConsumer;
    /**
     * 生产者发送信息
     * */
    @RequestMapping("/send")
    public void kafkaSend(){
        Message message = Message.builder()
                .msg("我是从KafkaMessageController 通过KafkaProducer.sendMessage() 方法发送过来的消息")
                .code("200")
                .sentTime("生产者发送消息")
                .sender("banyiyi")
                .build();
        kafkaProducer.sendMessage(message);
    }

    /**
     * 消费者读取信息
     */
    @RequestMapping("/read")
    public void kafkaRead(){
        kafkaConsumer.consume();
    }
}
