package com.study.banyiyi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
public class BanyiyiApplication {

    public static void main(String[] args) {
        SpringApplication.run(BanyiyiApplication.class, args);
    }

    @RequestMapping("/")
    public String hello(){
        return "hello,this is springboot-JVM project";
    }

}
