package com.study.banyiyi.util;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

/**
 * @ClassName Color
 * @Description 枚举类型
 * @Author yangwm
 * @Date 2021/7/8 17:34
 * @Version 1.0
 */
public enum Color {

    RED(1),
    BLUE(2),
    YELLOW(3);

    @Getter
    @Setter
    private int value;

    Color(int i) {
        this.value = i;
    }
}
