package com.study.banyiyi;

import com.study.banyiyi.util.Color;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;
import java.util.Iterator;

@SpringBootTest
@RunWith(SpringRunner.class)
class BanyiyiApplicationTests {

    /*JVM类加载测试*/
    @Test
    public void claseLoader(){
        ClassLoader loader = Thread.currentThread().getContextClassLoader();
        System.out.println(loader);
        System.out.println(loader.getParent());
        System.out.println(loader.getParent().getParent());
    }
    /* class类的加载方式*/
    @Test
    public void classLoaderType(){
        ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
        try {
            //使用Class.forName()
            //Class.forName("com.study.banyiyi.entry.Student");
            //Class.forName("com.study.banyiyi.entry.Student",false,classLoader);
            //使用Classloader.loadClass()
            classLoader.loadClass("com.study.banyiyi.entry.Student");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    @Test
    void getColor(){
        Color.RED.setValue(4);
        int value = Color.RED.getValue();
        Color[] values = Color.values();
        Iterator iterator = Arrays.asList(values).iterator();
        while (iterator.hasNext()){
            System.out.println(iterator.next());
        }
    }
}
