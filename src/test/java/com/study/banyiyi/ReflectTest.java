package com.study.banyiyi;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * @ClassName ReflectTest
 * @Description TODO
 * @Author yangwm
 * @Date 2021/7/28 14:38
 * @Version 1.0
 */
@RunWith(SpringRunner.class)
@SpringBootTest
class ReflectTest {

    @Test
    public void getConstructors(){
        try {
            Class clazz = Class.forName("com.study.banyiyi.entry.Student");
            //获取公开构造器
            Constructor[] constructors = clazz.getConstructors();
            System.out.println("----------------共有构造器----------------");
            for(Constructor c: constructors){
                System.out.println(c);
            }
            //获取所有的构造器
            Constructor[] allConstructors = clazz.getDeclaredConstructors();
            System.out.println("--------------私有构造器------------------");
            for(Constructor c:allConstructors){
                System.out.println(c);
            }
            //获取共有、私有构造器并执行
            Constructor constructor = clazz.getDeclaredConstructor(String.class);
            System.out.println("———————————————获取私有带参构造器———————————————");
            System.out.println(constructor);
            constructor.setAccessible(true);//暴力访问
            Object obj = constructor.newInstance("半一一");
            System.out.println(obj);
        } catch (ClassNotFoundException | NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
    }


    @Test
    public void getFeilds(){
        try {
            Class clazz = Class.forName("com.study.banyiyi.entry.Student");
            Field[] fields = clazz.getFields();
            System.out.println("-----------------获取共有变量-----------------");
            for(Field f:fields){
                System.out.println(f);
            }
            Field[] allField = clazz.getDeclaredFields();
            System.out.println("--------------获取全部变量---------------");
            for(Field f:allField){
                System.out.println(f);
            }
            Field field = clazz.getDeclaredField("name");
            System.out.println("-------------获取name变量--------------");
            System.out.println(field);
            field.setAccessible(true);//暴力访问
            field.set(clazz.getConstructor().newInstance(),"江湖");
            System.out.println(field);
        } catch (ClassNotFoundException | NoSuchFieldException | NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void getMethods(){
        try {
            Class clazz = Class.forName("com.study.banyiyi.entry.Student");
            Method[] methods = clazz.getMethods();
            System.out.println("--------------获取公共成员方法--------------");
            for(Method m: methods){
                System.out.println(m);
            }
            Method[] allMethods = clazz.getDeclaredMethods();
            System.out.println("--------------获取所有的成员方法---------------");
            for(Method m: allMethods){
                System.out.println(m);
            }
            Object obj = clazz.getConstructor().newInstance();
            System.out.println("----------获取指定的方法--------------");
            Method score = clazz.getDeclaredMethod("outPutMessage", String.class);
            score.setAccessible(true);
            Object o = score.invoke(obj, "这是我从测试类中方法传递的参数");
            System.out.println("返回的值："+o);

        } catch (ClassNotFoundException | NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
    }


}
